﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotation : MonoBehaviour {

	public float rotSpeed = 180f; //Creates an adjustble variable for the rotation speed of the enemies

	Transform player;

	// Update is called once per frame
	void Update () { //if the player is active, the enemies will be able to locate it
		if (player == null) {
			GameObject go = GameObject.Find ("PlayerShip");
			if (go != null){
				player = go.transform;
		}
	}

		if(player == null){ //if the player is not active, the enemies will stop chasing
		return;
	}

		Vector3 dir = player.position - transform.position; //Gives the enemy the players constant location so it will continue to follow the player
		dir.Normalize();
		float zAngle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90;
		Quaternion desiredRot = Quaternion.Euler (0, 0, zAngle);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, desiredRot, rotSpeed * Time.deltaTime);

	}
}