﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour 
{
	public Transform Player;
	public GameObject enemy; // Lets us add the enemy prefab to controller
	public GameObject hazard; // Lets us add the asteroid prefab to controller
	public Vector3 spawnValues; // Allows us to create the spawn locations
	public int hazardCount; // Creates an adjustable variable for the number of asteroids
	public int enemyCount; // Creates an adjustable variable for the number of enemies
	public float spawnWait; // Creates an adjustable variable for the time between hazard spawns
	public float startWait; // Creates an adjustable variable for the time allotted before hazards spawn in when the game starts
	public float waveWait; // Creates an adjustable variable for the time between waves
	private float playerPosition;


	void Start()
	{
		StartCoroutine( SpawnWaves ());
	}
		

	IEnumerator SpawnWaves () 
	{
		yield return new WaitForSeconds (startWait);
		while (true){
			
			yield return new WaitForSeconds (waveWait);

			//spawns enemies//
			for (int i = 0; i < enemyCount;i++) {        
				Vector3 rotationVector = new Vector3 (0, 0, 180); //Sets the enemy rotation down toward the player (not required since they follow)
				Quaternion spawnRotation = Quaternion.Euler (rotationVector);
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);  //Determines where hazards will spawn
				Instantiate (enemy, spawnPosition, spawnRotation); // Creates the enemies
				yield return new WaitForSeconds (spawnWait);
			}

			//spawns asteroids//
			for (int i = 0; i < hazardCount;i++) {       
				Vector3 rotationVector = new Vector3 (0, 0, 180); //sets the asteroid rotation down toward the player 
				Quaternion spawnRotation = Quaternion.Euler (rotationVector);
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);  //Determines where hazards will spawn
				Instantiate (hazard, spawnPosition, spawnRotation); //Creates the asteroids
				yield return new WaitForSeconds (spawnWait);
			}

			yield return new WaitForSeconds (waveWait);
		}
	}
}
	