﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour {


	public int health; // Creates an adjustable variable for health
	Vector3 startPosition;

	void Start()
	{
		if(health <= 0) 
		{
			Application.Quit();

		}	
		startPosition = transform.position; //Gathers the players starting position for reset upon death
	}

	void OnTriggerEnter2D() //Upon colliding with an enemy, asteroid, or the border the player will lose 1 health and reset to center of screen
	{
		health--;
		Reset ();
	}
	void Reset() //resets player position
	{
		transform.position = startPosition;
	}
	void Update(){ // destroy gameobject if their health value reaches 0
	if(health <= 0) 
	{
	Application.Quit();
	}	
}
}