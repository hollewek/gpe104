﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {
	public Vector3 bulletOffset = new Vector3 (0, 0.5f, 0); //Creates an adjustble variable for bullet spawn loacation (used to center on sprite)
	public GameObject bulletPrefab; //Allows the bullet prefab to be attached to the player ship gameobject
	public float fireDelay = 0.25f; //Creates an adjustble variable for the rate of fire with the cooldownTimer
	float cooldownTimer = 0;
	
	// Update is called once per frame
	void Update () {
		cooldownTimer -= Time.deltaTime;
		if(Input.GetButton("Fire1") && cooldownTimer <= 0 ){ // Allows the spacebar to fire a bullet (can be held down for continuous fire, change to GetButtonDown to disable this)
			cooldownTimer = fireDelay;

			Vector3 offset = transform.rotation * bulletOffset;

			GameObject bulletGO = (GameObject)Instantiate (bulletPrefab, transform.position + offset, transform.rotation); // Creates the bullet prefab at the offset location on the same layer as the player
			bulletGO.layer = gameObject.layer;
		}
	}
}