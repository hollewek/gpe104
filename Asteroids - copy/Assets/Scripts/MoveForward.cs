﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour {
	public float lifetime; //Creates an adjustable variable that determines how long the object will last
	public float maxSpeed; //Creates an adjustable variable for the speed of the object
	
	// Update is called once per frame
	void Update () { //Causes the object to move forward based on the set speed per second
		Vector3 pos = transform.position;
		Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime,0);
		pos += transform.rotation * velocity;
		transform.position = pos;
		lifetime -= Time.deltaTime;
		if (lifetime <= 0){
			Destroy (gameObject);
		}
	}
}
