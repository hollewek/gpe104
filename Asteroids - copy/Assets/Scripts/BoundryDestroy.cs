﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundryDestroy : MonoBehaviour {

	void OnTriggerEnter2D() { //Destroys objects that do not have the Boundry tag
		if (tag != "Boundry") 
		{
			Destroy (gameObject);
		}

	}
}