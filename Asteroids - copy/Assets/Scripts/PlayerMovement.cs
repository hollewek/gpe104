﻿	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class PlayerMovement : MonoBehaviour {

	public float maxSpeed; 
	public float rotSpeed;

		
		// Update is called once per frame
		void Update () 
		{
		//Rotation ship movement
		Quaternion rot = transform.rotation;
		float z = rot.eulerAngles.z;
		z -= Input.GetAxis ("Horizontal") * rotSpeed * Time.deltaTime;
		rot = Quaternion.Euler (0, 0, z);
		transform.rotation = rot;

		//Vertical ship movement
		Vector3 pos = transform.position;
		Vector3 velocity = new Vector3(0, Input.GetAxis ("Vertical") * maxSpeed * Time.deltaTime,0);
		pos += rot * velocity;

		transform.position = pos;

		}
			

	}
