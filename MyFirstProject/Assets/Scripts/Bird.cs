﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour 
{

	private Rigidbody2D rb2d; //Gives access to rigid body variables
	public float speed; //Gives access to speed variable for controller

	// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () 
	{
		// adds a controller to move left and right with, and up and down

		float moveHorizontal = Input.GetAxis ("Horizontal"); 
		float moveVertical = Input.GetAxis ("Vertical");

		// Vertical movement may be replaced with jump function so it is set to 0
		Vector2 movement = new Vector2 (moveHorizontal, 0.0f);

		rb2d.AddForce (movement * speed);

	}

}
